import { useSelector } from "react-redux"

function CarValue() {

  const totalValue = useSelector(({ cars: { data, searchTerm } }) => {
    return data
      .filter((car) => car.name.toLowerCase().includes(searchTerm.toLowerCase()))
      .reduce((acc, car) => acc + parseInt(car.value), 0)
  })

  return (
    <div className="car-value">
      Total Value: ${totalValue}
    </div>
  )
}

export default CarValue
