import { useDispatch, useSelector } from "react-redux"
import { changeName, changeValue, addCar } from "../store"

function CarForm() {

  const dispatch = useDispatch()

  const { name, value } = useSelector((state) => {
    return {
      name: state.form.name,
      value: state.form.value
    }
  })

  const handleNameChange = (event) => {
    dispatch(changeName(event.target.value))
  }

  const handleValueChange = (event) => {
    const carValue = parseInt(event.target.value) || 0
    dispatch(changeValue(carValue))
  }

  const handleSubmit = (event) => {
    event.preventDefault()
    dispatch(addCar({
      name,
      value
    }))
  }

  return (
    <div className="car-form panel">
      <h3 className="subtitle is-3">Add Car</h3>
      <form onSubmit={handleSubmit}>
        <div className="field-group">
          <div className="field">
            <label className="label">Name</label>
            <input
              className="input is-expanded"
              value={name}
              onChange={handleNameChange}
              type="text"
            />
          </div>
          <div className="field">
            <label className="label">Value</label>
            <input
              className="input is-expanded"
              value={value || ''}
              onChange={handleValueChange}
              type="number"
            />
          </div>
          <div className="field">
            <button className="button is-link">Submit</button>
          </div>
        </div>
      </form>
    </div>
  )
}

export default CarForm
