import { createSlice, nanoid } from "@reduxjs/toolkit"

const carsSlice = createSlice({
  name: 'cars',
  initialState: {
    searchTerm: '',
    data: []
  },
  reducers: {
    changeSearchTerm(state, action) {
      state.searchTerm = action.payload
    },
    addCar(state, action) {
      // Assumption:
      // action.payload === { name: 'example', cost: 100 }
      state.data.push({
        name: action.payload.name,
        value: action.payload.value,
        id: nanoid(),
      })
    },
    removeCar(state, action) {
      // Assumption:
      // action.payload === the id of the car we want to remove
      state.data = state.data.filter((car) => {
        return car.id !== action.payload
      })
    }
  }
})

export const { changeSearchTerm, addCar, removeCar } = carsSlice.actions
export const carsReducer = carsSlice.reducer
